const Webhook = require('svix').Webhook;

const subscribeHook = (z, bundle) => {
    return z.request({
            url: `https://api.svix.com/api/v1/app/${bundle.authData.application_id}/endpoint/`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json',
                'Authorization': `Bearer ${bundle.authData.integration_key}`
            },
            body: {
                'url': bundle.targetUrl,
                'version': '1',
                'rateLimit': 10,
            }
        })
        .then((createEndpointResponse) => {
            createEndpointResponse.throwForStatus();

            const endpointId = createEndpointResponse.json['id'];

            return z.request({
                    url: `https://api.svix.com/api/v1/app/${bundle.authData.application_id}/endpoint/${endpointId}/secret/`,
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'accept': 'application/json',
                        'Authorization': `Bearer ${bundle.authData.integration_key}`
                    }
                })
                .then((getSecretResponse) => {
                    getSecretResponse.throwForStatus();

                    const secretKey = getSecretResponse.json['key']

                    return {
                        "id": endpointId,
                        "secret": secretKey
                    };
                });
        });
};

const unsubscribeHook = (z, bundle) => {
    return z.request({
            url: `https://api.svix.com/api/v1/app/${bundle.authData.application_id}/endpoint/${bundle.subscribeData.id}/`,
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json',
                'Authorization': `Bearer ${bundle.authData.integration_key}`
            },
            body: {}
        })
        .then((response) => {
            response.throwForStatus();
            return response.json;
        });
};

const listEvents = (z, bundle) => {
  return z.request({
    url: `https://api.svix.com/api/v1/app/${bundle.authData.application_id}/msg/?limit=1`,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'accept': 'application/json',
      'Authorization': `Bearer ${bundle.authData.integration_key}`
    },
    body: {

    }
  })
    .then((response) => {
      response.throwForStatus();
      const firstMessage = response.json.data[0];
      if (!firstMessage) {
        throw Error("No messages found for this event type.");
      }
      return z.request({
        url: `https://api.svix.com/api/v1/app/${bundle.authData.application_id}/msg/${firstMessage.id}/`,
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'accept': 'application/json',
          'Authorization': `Bearer ${bundle.authData.integration_key}`
        },
        body: {
        }
      }).then(fullMsg => {
        return [fullMsg.json.payload];
      })
    });
};

const getMessage = (z, bundle) => {
    const headers = {
        "svix-id": bundle.rawRequest.headers["Http-Svix-Id"],
        "svix-timestamp": bundle.rawRequest.headers["Http-Svix-Timestamp"],
        "svix-signature": bundle.rawRequest.headers["Http-Svix-Signature"],
    };
    const wh = new Webhook(bundle.subscribeData.secret);
    wh.verify(bundle.rawRequest.content, headers);
    return [bundle.cleanedRequest];
};

module.exports = {
    key: 'message_received',
    noun: 'Message',
    display: {
        label: 'Receive All Messages',
        description: 'Triggers when any webhook message is received.',
        hidden: true, // We keep it hidden because Zapier doesn't support publishing it. Just keep it here as an example.
    },
    operation: {
        type: 'hook',
        performSubscribe: subscribeHook,
        performUnsubscribe: unsubscribeHook,
        performList: listEvents,
        perform: getMessage,
    },
};